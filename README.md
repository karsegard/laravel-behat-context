# This is my package laravel-behat-context

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-behat-context.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-behat-context)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-behat-context.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-behat-context)

Base context for behat in laravel